﻿Sample configuration file.
--------------------------

<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <configSections>
    <section name="RToggle" type="RToggle.Repository.ConfigurationSection.Config.RToggleSection, RToggle.Repository.ConfigurationSection" />
  </configSections>
  <RToggle>
    <standardFeatures>
      <feature name="TestTrue" isEnabled="true" isEstablished="false" />
      <feature name="TestFalse" isEnabled="false" isEstablished="false" />
      <feature name="TestTrueWithEstablished" isEnabled="true" isEstablished="true" />
      <feature name="TestFalseWithEstablished" isEnabled="false" isEstablished="true" />
    </standardFeatures>
    <timedFeatures>
      <timedFeature name="TimedFeatureOne" startDateTime="2008-05-01T07:34:42Z" endDateTime="2020-05-01T07:34:42Z" isEstablished="True"/>
      <timedFeature name="TimedFeatureTwo" startDateTime="2020-01-01T01:00:00Z" endDateTime="2020-05-01T07:34:42Z" isEstablished="false"/>
    </timedFeatures>
  </RToggle>
</configuration>