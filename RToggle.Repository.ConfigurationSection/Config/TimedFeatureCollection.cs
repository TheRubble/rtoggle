﻿using System.Configuration;

namespace RToggle.Repository.ConfigurationSection.Config
{
    public class TimedFeatureCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new TimedFeatureElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TimedFeatureElement)element).Name;
        }

    }
}
