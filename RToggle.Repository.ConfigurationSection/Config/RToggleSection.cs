﻿using System.Configuration;

namespace RToggle.Repository.ConfigurationSection.Config
{
    public class RToggleSection : System.Configuration.ConfigurationSection
    {
        [ConfigurationProperty("standardFeatures", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(FeatureCollection), AddItemName = "feature")]
        public FeatureCollection StandardFeatures
        {
            get
            {
                return (FeatureCollection)base["standardFeatures"];
            }
        }

        [ConfigurationProperty("timedFeatures", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(TimedFeatureCollection), AddItemName = "timedFeature")]
        public TimedFeatureCollection TimedFeatures
        {
            get
            {
                return (TimedFeatureCollection)base["timedFeatures"];
            }
        }

    }
}
