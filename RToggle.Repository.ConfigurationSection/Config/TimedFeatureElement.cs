﻿using System;
using System.Configuration;

namespace RToggle.Repository.ConfigurationSection.Config
{
    public class TimedFeatureElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)this["name"]; }
        }

        [ConfigurationProperty("isEstablished", IsRequired = false)]
        public bool IsEstablished
        {
            get { return (bool)this["isEstablished"]; }
        }

        [ConfigurationProperty("startDateTime", IsRequired = true, IsKey = false)]
        public DateTime StartDateTime
        {
            get { return (DateTime)this["startDateTime"]; }
        }

        [ConfigurationProperty("endDateTime", IsRequired = true, IsKey = false)]
        public DateTime EndDateTime
        {
            get { return (DateTime)this["endDateTime"]; }
        }
    }
}
