﻿using System.Configuration;

namespace RToggle.Repository.ConfigurationSection.Config
{
    public class FeatureCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new FeatureElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FeatureElement)element).Name;
        }

    }
}
