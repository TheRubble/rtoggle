﻿using System.Configuration;

namespace RToggle.Repository.ConfigurationSection.Config
{
    public class FeatureElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)this["name"]; }
        }

        [ConfigurationProperty("isEnabled", IsRequired = true)]
        public bool IsEnabled
        {
            get { return (bool)this["isEnabled"]; }
        }

        [ConfigurationProperty("isEstablished", IsRequired = false)]
        public bool IsEstablished
        {
            get { return (bool)this["isEstablished"]; }
        }
    }
}
