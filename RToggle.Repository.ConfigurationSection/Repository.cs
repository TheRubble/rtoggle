﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RToggle.Repository.ConfigurationSection.Config;
using RToggle.Shared;
using RToggle.Shared.Base;
using RToggle.Shared.Features;

namespace RToggle.Repository.ConfigurationSection
{
    public class Repository : FeatureRepositoryBase
    {
        private readonly RToggleSection _section;

        public Repository()
        {
            _section = (RToggleSection)ConfigurationManager.GetSection("RToggle");
            
            if (_section == null)
            {
                throw new ConfigurationInvalidException("Could not load the configuration section");
            }

        }

        public override IFeature GetFeature(string feature)
        {

            if (string.IsNullOrEmpty(feature))
                throw new ArgumentException(feature);

            // Extend here for additional feature types.
            var features =  GetStandardFeatures()
                            .Union(GetTimedFeatures());
            
            var subject = (from FeatureBase f in features
                           where f.Name.Equals(feature,StringComparison.InvariantCultureIgnoreCase)
                           select f).FirstOrDefault();

            return subject;
        }

        private IEnumerable<FeatureBase> GetTimedFeatures()
        {

            return from TimedFeatureElement tf in _section.TimedFeatures
                select new TimedFeature(tf.Name, tf.IsEstablished, tf.StartDateTime, tf.EndDateTime);
        }

        private IEnumerable<FeatureBase> GetStandardFeatures()
        {
            return from FeatureElement f in _section.StandardFeatures
                   select new Feature(f.Name,f.IsEstablished,f.IsEnabled);
        }

    }
}
