﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using RToggle.Shared;
using RToggle.Shared.Features;

namespace RToggle.Core.UnitTest
{
    [TestFixture]
    public class MultiRepositoryEngineTests
    {
        private IFeatureEngine _featureEngine;
        private Mock<IFeatureRepository> _featureRepo;
        private Mock<IFeatureRepository> _featureSecondRepo;

        private Mock<IEstablishedNotification> _establishedMoq;


        [SetUp]
        public void Setup()
        {
            _featureRepo = new Mock<IFeatureRepository>();
            _featureSecondRepo = new Mock<IFeatureRepository>();
            _establishedMoq = new Mock<IEstablishedNotification>();
            _featureEngine = new FeatureEngine(new[]
                {
                 _featureRepo.Object,
                 _featureSecondRepo.Object
                },_establishedMoq.Object);
        }

        [Test]
        public void BothRepositoriesHaveFeature_FirstIsReturned()
        {
            _featureRepo.Setup(r => r.GetFeature("Test")).Returns(new Feature("Test", true, true)).Verifiable();
            _featureSecondRepo.Setup(r => r.GetFeature("Test")).Returns(new Feature("Test", true, true));

            var subject = _featureEngine.GetFeature("Test");
            Assert.IsNotNull(subject);

            _featureRepo.Verify(r => r.GetFeature("Test"), Times.Once);
            _featureSecondRepo.Verify(r => r.GetFeature("Test"), Times.Never);

        }

        [Test]
        public void FeatureOnlyInFirstRepository_FeatureIsReturned()
        {
            _featureRepo.Setup(r => r.GetFeature("Test")).Returns(new Feature("Test", true, true)).Verifiable();

            var subject = _featureEngine.GetFeature("Test");
            Assert.IsNotNull(subject);

            _featureRepo.Verify(r => r.GetFeature("Test"), Times.Once);
            _featureRepo.Verify();

        }

        [Test]
        public void FeatureOnlyAvailableInSecondRepository_FeatureIsReturned()
        {
            _featureRepo.Setup(r => r.GetFeature("Test")).Returns((IFeature) null).Verifiable();
            _featureSecondRepo.Setup(r => r.GetFeature("Test")).Returns(new Feature("Test", true, true)).Verifiable();

            var subject = _featureEngine.GetFeature("Test");
            Assert.IsNotNull(subject);

            _featureRepo.Verify();
            _featureSecondRepo.Verify();
        }

        [Test]
        public void FeatureUnavailableInAllRepositories_DisabledFeatureReturned()
        {
            var subject = _featureEngine.GetFeature("Test");
            Assert.IsNotNull(subject);
            Assert.IsFalse(subject.Enabled);
            Assert.IsFalse(subject.Established);
            Assert.AreEqual(subject.Name,"Test");
            
        }

        [TearDown]
        public void TearDown()
        {
            _featureRepo = null;
            _featureEngine = null;
        }
    }
}
