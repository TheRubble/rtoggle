﻿using System;
using System.CodeDom;
using Moq;
using NUnit.Framework;
using RToggle.Shared;
using RToggle.Shared.Features;

namespace RToggle.Core.UnitTest
{
	[TestFixture]
	public class EstablishedNotificationTests
	{
		private EstablishedNotification _subject;
	    private Mock<IEstablishedObserver> _observer;

		[SetUp]
		public void SetUp()
		{
			_subject = new EstablishedNotification ();
		    _observer = new Mock<IEstablishedObserver>();
		}
        
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CannotSubscribeNullObserver()
		{
			_subject.Register (null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CannotUnsubscribeNullObserver()
		{
			_subject.Deregister (null);
		}

	    [Test]
	    public void CheckNotifyCalledOnObserver()
	    {
	        //var observer = new Mock<IEstablishedObserver>();
	        _observer.Setup(s => s.Notify(It.IsAny<IFeature>()));

            _subject.Register(_observer.Object);
	        _subject.Notify(new Feature("Feature",false,false));

            _observer.Verify(s => s.Notify(It.IsAny<IFeature>()),Times.Once);
	    }

	    [Test]
	    public void CheckUnsubscribe()
	    {
	        _observer.Setup(o => o.Notify(It.IsAny<IFeature>()));
	        _subject.Register(_observer.Object);
            _subject.Deregister(_observer.Object);
            _subject.Notify(new Feature("Feature", false, true));
            _observer.Verify(o => o.Notify(It.IsAny<IFeature>()),Times.Never);
            
	    }
	}
}

