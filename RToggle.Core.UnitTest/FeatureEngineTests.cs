﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using RToggle.Shared;
using RToggle.Shared.Features;


namespace RToggle.Core.UnitTest
{

    public enum TestEnum
    {
        TestFeature
    }

    [TestFixture]
    public class FeatureEngineTests
    {
        private IFeatureEngine _featureEngine;
        private Mock<IFeatureRepository> _featureMoq;
        private Mock<IEstablishedNotification> _establishedMoq;

        

        [SetUp]
        public void Setup()
        {
            _featureMoq = new Mock<IFeatureRepository>();
            _establishedMoq = new Mock<IEstablishedNotification>();
            _featureEngine = new FeatureEngine(_featureMoq.Object,_establishedMoq.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _featureMoq = null;
            _featureEngine = null;
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullRepositoryShouldThrowException()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new FeatureEngine(repository: null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullRepositoriesShouldThrowException()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new FeatureEngine(repositories: null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullEstablishedNotification()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new FeatureEngine(_featureMoq.Object, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CallGetFeatureWithEmptyStringShouldThrow()
        {
			_featureEngine.GetFeature(string.Empty);
        }

        [Test]
        public void CallGetFeatureWithValidEnumExpectFeature()
        {
            _featureMoq
				.Setup(m => m.GetFeature("Feature"))
					.Returns(new Feature("Feature",false,true));

            var result = _featureEngine.GetFeature("Feature");
            Assert.IsTrue(result.Enabled);

            _featureMoq.VerifyAll();
        }

        [Test]
        public void CallGetFeatureEstablishedWithValidEnumExpectFeature()
        {
            _featureMoq
                .Setup(m => m.GetFeature("Feature"))
                .Returns(new Feature("Feature", true, true));

            var result = _featureEngine.GetFeature("Feature");
            Assert.IsTrue(result.Established);

            _featureMoq.VerifyAll();
        }

        [Test]
        public void CallFeatureThatIsntConfigured_ExpectDisabledFeature()
        {
            _featureMoq
                .Setup(m => m.GetFeature("NullFeature"))
                .Returns((IFeature) null);

            var result = _featureEngine.GetFeature("NullFeature");
            Assert.IsFalse(result.Established);
            Assert.IsFalse(result.Enabled);
            Assert.AreEqual(result.Name,"NullFeature");

            _featureMoq.VerifyAll();
        }

        [Test]
        public void CallRegisterForEstablishedNotification()
        {
            var mockObserver = new Mock<IEstablishedObserver>();
            _establishedMoq.Setup(e => e.Register(mockObserver.Object)).Verifiable();
            _featureEngine.RegisterForEstablisedNotification(mockObserver.Object);
            _establishedMoq.Verify();
        }

        [Test]
        public void CallDeregisterForEstablishedNotification()
        {
            var mockObserver = new Mock<IEstablishedObserver>();
            _establishedMoq.Setup(e => e.Deregister(mockObserver.Object)).Verifiable();
            _featureEngine.DeregisterForEstablishedNotification(mockObserver.Object);
            _establishedMoq.Verify();
        }

    }

   
}
