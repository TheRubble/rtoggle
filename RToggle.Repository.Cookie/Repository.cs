﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RToggle.Shared;
using RToggle.Shared.Base;
using RToggle.Shared.Features;

namespace RToggle.Repository.Cookie
{
    public class Repository : FeatureRepositoryBase
    {

        private readonly HttpContextBase _context;

        public Repository(HttpContextBase context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;
        }

        public override IFeature GetFeature(string feature)
        {
            if (string.IsNullOrEmpty(feature))
                throw new ArgumentException(feature);

            var cookie = _context.Request.Cookies[feature];

            if (cookie == null || string.IsNullOrEmpty(cookie.Value))
            {
                return null;
            }

            try
            {
                var json = JObject.Parse(cookie.Value);
                var jsonType = json["Type"];
                
                switch (jsonType.ToString().ToUpperInvariant())
                {
                    case "RTOGGLE.SHARED.FEATURES.TIMEDFEATURE":
                        return JsonConvert.DeserializeObject<SerialisedFeatureContainer<TimedFeature>>(cookie.Value).Feature;
                    case "RTOGGLE.SHARED.FEATURES.FEATURE":
                        return JsonConvert.DeserializeObject<SerialisedFeatureContainer<Feature>>(cookie.Value).Feature;
                }
            }
            catch (JsonReaderException)
            {
                
            }

            return null;

        }

    }
}
