﻿using System;
using RToggle.Core;
using RToggle.Repository.ConfigurationSection;
using RToggle.Shared;

namespace RToggle.Sample.ConsoleApplication
{
    class Program
    {

        private static IFeatureRepository _repo;
        private static IFeatureEngine _engine;

        static void Main(string[] args)
        {
            _repo = new Repository.ConfigurationSection.Repository();
            _engine = new FeatureEngine(_repo);

            

            Console.WriteLine("Press 1 : For enabled feature");
            Console.WriteLine("Press 2 : For disabled feature");
            Console.WriteLine("--------------------------");
            var response = Console.ReadKey();

            switch (response.Key)
            {
                case ConsoleKey.D1:
                    {
                        EnabledFeature();
                        break;
                    }
                default:
                    {
                        DisabledFeature();
                        break;
                    }
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

        }

        static void EnabledFeature()
        {
            var feature = _engine.GetFeature("Enabled");
            if (!feature.Enabled) return;

            Console.WriteLine();
            Console.WriteLine("An enabled feature.");
        }

        static void DisabledFeature()
        {
            var feature = _engine.GetFeature("Disabled");
            if (feature.Enabled) return;

            Console.WriteLine();
            Console.WriteLine("A disabled feature.");
        }
    }
}
