﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RToggle.Shared.Base
{
    public abstract class FeatureRepositoryBase : IFeatureRepository
    {
        public abstract IFeature GetFeature(string feature);
    }
}
