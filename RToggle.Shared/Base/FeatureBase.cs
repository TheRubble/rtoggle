﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RToggle.Shared.Base
{
    public abstract class FeatureBase : IFeature
    {

        private readonly string _name;
        private readonly bool _established;

        protected FeatureBase(string name, bool established)
        {
            _name = name;
            _established = established;
        }

        public string Name
        {
            get { return _name; }
        }

        public abstract bool Enabled { get; }

        public bool Established
        {
            get { return _established; }
        }
    }
}
