﻿using JetBrains.Annotations;

namespace RToggle.Shared
{
    public interface IFeatureRepository
    {
        IFeature GetFeature(string feature);
    }
}
