﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RToggle.Shared
{
    [Serializable]
    public class ConfigurationInvalidException : Exception
    {
        public ConfigurationInvalidException()
            : base()
        {
        }

        public ConfigurationInvalidException(String message)
            : base(message)
        {
        }

        public ConfigurationInvalidException(String message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ConfigurationInvalidException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
