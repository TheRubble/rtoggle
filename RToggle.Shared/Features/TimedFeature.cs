﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using RToggle.Shared.Base;

namespace RToggle.Shared.Features
{
    public class TimedFeature : FeatureBase
    {
        private readonly DateTime _startDateTime;
        private readonly DateTime _endDateTime;

        public TimedFeature(string name, bool established, DateTime startDateTime, DateTime endDateTime)
            : base(name, established)
        {
            _startDateTime = startDateTime;
            _endDateTime = endDateTime;
        }

        public override bool Enabled
        {
            get
            {
                var now = DateTime.Now;
                if (now.CompareTo(_startDateTime) >= 0 && now.CompareTo(_endDateTime) <= 0)
                {
                    return true;
                }
                return false;
            }
        }

        public DateTime StartDateTime
        {
            get
            {
                return _startDateTime;
            }
        }

        public DateTime EndDateTime
        {
            get
            {
                return _endDateTime;
            }
        }

    }
}
