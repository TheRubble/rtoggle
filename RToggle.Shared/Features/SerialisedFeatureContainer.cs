﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using RToggle.Shared.Base;

namespace RToggle.Shared.Features
{
    [Serializable]
    public class SerialisedFeatureContainer<T> where T : FeatureBase
    {
        private readonly T _feature;

        public SerialisedFeatureContainer(T feature)
        {
            _feature = feature;
        }

        [DataMember]
        public string Type
        {
            get { return typeof (T).ToString(); }
        }

        [DataMember]
        public T Feature
        {
            get { return _feature; }
        }
    }
}
