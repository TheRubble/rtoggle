﻿using RToggle.Shared.Base;

namespace RToggle.Shared.Features
{
    public class Feature : FeatureBase
    {

        private readonly bool _enabled;

        public Feature(string name, bool established, bool enabled)
            : base(name, established)
        {
            _enabled = enabled;
        }

        public override bool Enabled
        {
            get { return _enabled; }
        }

        
    }
}
