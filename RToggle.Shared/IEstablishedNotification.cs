﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RToggle.Shared
{
    public interface IEstablishedNotification
    {
        void Register(IEstablishedObserver observer);
        void Deregister(IEstablishedObserver observer);
        void Notify(IFeature feature);
    }
}
