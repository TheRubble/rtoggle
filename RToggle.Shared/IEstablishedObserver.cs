﻿using System;

namespace RToggle.Shared
{
	public interface IEstablishedObserver
	{
		void Notify(IFeature feature);
	}
}