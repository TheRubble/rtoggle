﻿using JetBrains.Annotations;

namespace RToggle.Shared
{
    /// <summary>
    /// The FeatureEngine interface.
    /// </summary>
    public interface IFeatureEngine
    {
		[NotNullAttribute]
        IFeature GetFeature([NotNull]string featureKey);
        void RegisterForEstablisedNotification([NotNull]IEstablishedObserver observer);
        void DeregisterForEstablishedNotification([NotNull]IEstablishedObserver observer);
    }
}
