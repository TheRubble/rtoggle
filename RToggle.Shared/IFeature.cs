﻿using JetBrains.Annotations;

namespace RToggle.Shared
{
    public interface IFeature
    {
        string Name { get; }
        bool Enabled { get; }
        bool Established { get;}
    }
}
