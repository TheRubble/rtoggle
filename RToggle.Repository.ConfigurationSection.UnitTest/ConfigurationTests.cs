﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RToggle.Repository.ConfigurationSection.Config;

namespace RToggle.Repository.ConfigurationSection.UnitTest
{
    [TestFixture]
    public class ConfigurationTests
    {

        private RToggleSection _section;

        [SetUp]
        public void Setup()
        {
            _section = (RToggleSection)ConfigurationManager.GetSection("RToggle");
        }


        [Test]
        public void CanLoadconfigurationSection()
        {
            Assert.IsNotNull(_section);
        }

        [Test]
        public void HasValidStandardConfigurationItems()
        {
            Assert.IsTrue(_section.StandardFeatures.Count == 4);
        }

        [Test]
        [TestCase(false,true)]
        [TestCase(false, false)]
        [TestCase(true, true)]
        [TestCase(true, false)]
        public void CheckForCorrectStandardFeatureValuesNoEstablished(bool isEstablished, bool isEnabled)
        {
            var subject = (from FeatureElement f in _section.StandardFeatures
                where f.IsEstablished == isEstablished
                && f.IsEnabled == isEnabled
                select f);

            Assert.IsNotNull(subject);
            Assert.IsTrue(subject.Count()== 1 );
            
        }
    }
}
