﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RToggle.Shared;
using RToggle.Shared.UnitTest;

namespace RToggle.Repository.ConfigurationSection.UnitTest
{
    [TestFixture]
    public class ConfigurationRepositoryTests : FeatureRepositoryTestBase
    {
        public override IFeatureRepository GetRepository()
        {
            return new Repository();
        }
    }
}
