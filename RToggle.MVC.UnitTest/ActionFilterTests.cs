﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Moq;
using NUnit.Framework;
using RToggle.Shared;
using RToggle.Shared.Features;

namespace RToggle.MVC.UnitTest
{

    public enum TestFeature
    {
        FeatureOne,
        FeatureTwo
    }

    [TestFixture]
    public class ActionFilterTests
    {

        private Mock<IFeatureEngine> _featureEngineMock;

        [SetUp]
        public void Setup()
        {
            _featureEngineMock = new Mock<IFeatureEngine>();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateFilterWithNoEngineExpectException()
        {
            Func<IFeature,ActionResult> featureDisabled = (IFeature feature) =>
            {
                return new ViewResult();
            };

            new RToggleActionFilter(null, featureDisabled);

        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateFilterWithOnFeatureDisabledExpectException()
        {
            new RToggleActionFilter(_featureEngineMock.Object, null);
        }

        [Test]
        public void CreateFilterWithValidParams()
        {
            Func<IFeature, ActionResult> featureDisabled = (IFeature feature) =>
            {
                return new ViewResult();
            };

            new RToggleActionFilter(_featureEngineMock.Object, featureDisabled);
        }

        [Test]
        public void CheckCompletesWithValidEnabledFeature()
        {
            Func<IFeature, ActionResult> featureDisabled = (IFeature feature) => {return new ViewResult();};
            var actionContext = new Mock<ActionExecutingContext>();

            actionContext.Setup(ctx => ctx.ActionDescriptor.GetCustomAttributes(true))
            .Returns(new object[]
            {
                new RToggleFeatureAttribute("Feature")
            });

            _featureEngineMock.Setup(fe => fe.GetFeature("Feature")).Returns(new Feature("Feature", false, true));
            
            var subject = new RToggleActionFilter(_featureEngineMock.Object, featureDisabled);

            subject.OnActionExecuting(actionContext.Object);

        }

        [Test]
        public void CheckSuppliedFuncCalledOnDisabled()
        {
            var called = false;

            Func<IFeature, ActionResult> featureDisabled = (IFeature feature) =>
            {
                called = true;
                return new ViewResult();
            };
            var actionContext = new Mock<ActionExecutingContext>();

            actionContext.Setup(ctx => ctx.ActionDescriptor.GetCustomAttributes(true))
            .Returns(new object[]
            {
                new RToggleFeatureAttribute("Feature")
            });

            _featureEngineMock.Setup(fe => fe.GetFeature("Feature")).Returns(new Feature("Feature", false, false));

            var subject = new RToggleActionFilter(_featureEngineMock.Object, featureDisabled);
            subject.OnActionExecuting(actionContext.Object);

            Assert.IsTrue(called);

        }
    }
}
