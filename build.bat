@echo Off
set config=%1
if "%config%" == "" (
   set config=Release
)
 
set version=1.0.0
if not "%PackageVersion%" == "" (
   set version=%PackageVersion%
)

set nuget=
if "%nuget%" == "" (
	set nuget=nuget
)

set nunit="Tools\nunit\nunit-console.exe"

"%MSBUILDEXE%" rToggle.sln /p:Configuration="%config%" /m /v:M /fl /flp:LogFile=msbuild.log;Verbosity=diag /nr:false /p:VisualStudioVersion=12.0

REM Unit tests
%nunit% RToggle.Repository.Cookie.UnitTest\bin\%config%\RToggle.Repository.Cookie.UnitTest.dll /framework:net-4.5
if not "%errorlevel%"=="0" exit -1
%nunit% RToggle.Repository.ConfigurationSection.UnitTest\bin\%config%\RToggle.Repository.ConfigurationSection.UnitTest.dll /framework:net-4.5
if not "%errorlevel%"=="0" exit -1
%nunit% RToggle.Repository.ConfigurationSection.UnitTest\bin\%config%\RToggle.Shared.UnitTest.dll /framework:net-4.5
if not "%errorlevel%"=="0" exit -1
%nunit% RToggle.Core.UnitTest\bin\%config%\RToggle.Core.UnitTest.dll /framework:net-4.5
if not "%errorlevel%"=="0" exit -1

mkdir Build

.nuget\nuget.exe pack "RToggle.Shared.nuspec" -NoPackageAnalysis -verbosity detailed -o Build -Version %version% -p Configuration="%config%" -symbols -Exclude **\obj\**
.nuget\nuget.exe pack "RToggle.Core.nuspec" -NoPackageAnalysis -verbosity detailed -o Build -Version %version% -p Configuration="%config%" -symbols -Exclude **\obj\**
.nuget\nuget.exe pack "RToggle.Repository.ConfigurationSection.nuspec" -NoPackageAnalysis -verbosity detailed -o Build -Version %version% -p Configuration="%config%" -symbols -Exclude **\obj\**
.nuget\nuget.exe pack "RToggle.Repository.Cookie.nuspec" -NoPackageAnalysis -verbosity detailed -o Build -Version %version% -p Configuration="%config%" -symbols -Exclude **\obj\**




