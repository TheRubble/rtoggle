# README #

RToggle is a basic extensible feature toggling library. Currently the only feature source available is based around configuration sections, I hope to change this shortly.

### Features ###

Currently two features are implemented :

* Standard features - Basic bool enabled or disabled.
* Timed features - Enabled between two dates.

### Established features ###

Once a feature is deemed to be established and beyond the point for which it could be safely switched off, marking it as established will enable established notifications to be raised. This gives an opportunity to deal with this requirement.

## Getting Started ##
View the wiki [https://bitbucket.org/TheRubble/rtoggle/wiki/Home]