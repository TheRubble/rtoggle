﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using RToggle.Shared;
using RToggle.Shared.Features;
using RToggle.Shared.UnitTest;

namespace RToggle.Repository.Cookie.UnitTest
{

    public class CookieRepositoryTests : FeatureRepositoryTestBase
    {
        private readonly Mock<HttpContextBase> _moqHttpContext;
        private readonly HttpCookieCollection _moqCollection = new HttpCookieCollection();

        public CookieRepositoryTests()
        {
            _moqHttpContext = new Mock<HttpContextBase>();
            var moqRequest = new Mock<HttpRequestBase>();

            _moqHttpContext.Setup(c => c.Request).Returns(moqRequest.Object);
            
            // Configure the test data
            ConfigureTestData();
            _moqHttpContext.Setup(c => c.Request.Cookies).Returns(_moqCollection);

        }

        public override IFeatureRepository GetRepository()
        {

            return new Repository(_moqHttpContext.Object);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullHttpContext()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Repository(null);
        }

        [Test]
        public void EmptyValueInCookie_ExpectNull()
        {
            CheckFeaturesIsNull("EmptyValue", false, false);
        }

        [Test]
        public void IncorrectTypeSerialised_ExpectNull()
        {
            CheckFeaturesIsNull("IncorrectType", false, false);
        }

        [Test]
        public void InvalidType_ExpectNull()
        {
            CheckFeaturesIsNull("OddTypes", false, false);
        }

        [Test]
        public void TypeEmpty_ExpectNull()
        {
            CheckFeaturesIsNull("OddTypes", false, false);
        }


        private void ConfigureTestData()
        {
            _moqCollection.Add(ConfigureStandardFeatureCookie("TestTrue",true,false));
            _moqCollection.Add(ConfigureStandardFeatureCookie("TestTrueWithEstablished", true, true));

            _moqCollection.Add(ConfigureTimedFeatureCookie(
                "TimedFeatureOne", 
                true, 
                DateTime.Parse("2008-05-01T07:34:42Z"),
                DateTime.Parse("2020-05-01T07:34:42Z")));

            _moqCollection.Add(ConfigureTimedFeatureCookie(
               "TimedFeatureTwo",
               false,
               DateTime.Parse("2020-01-01T01:00:00Z"),
               DateTime.Parse("2020-05-01T07:34:42Z")));

            _moqCollection.Add(new HttpCookie("EmptyValue",string.Empty));
            _moqCollection.Add(new HttpCookie("IncorrectType", JsonConvert.SerializeObject(1)));
            _moqCollection.Add(new HttpCookie("OddTypes", JsonConvert.SerializeObject(new
            {
                Type = "BustType.Broken"
            })));
            _moqCollection.Add(new HttpCookie("TypeEmpty", JsonConvert.SerializeObject(new
            {
                Type = string.Empty
            })));

            
        }

        private HttpCookie ConfigureStandardFeatureCookie(string name, bool enabled, bool established)
        {
            var featureContainer = new SerialisedFeatureContainer<Feature>(new Feature(name, established, enabled));
            return new HttpCookie(name, JsonConvert.SerializeObject(featureContainer, Formatting.None));

        }

        private HttpCookie ConfigureTimedFeatureCookie(string name, bool established, DateTime startDateTime, DateTime endDatetime)
        {

            var featureContainer = new SerialisedFeatureContainer<TimedFeature>( new TimedFeature(name, established, startDateTime, endDatetime));
          
            return new HttpCookie(name, JsonConvert.SerializeObject(featureContainer,Formatting.None));
        }

    }
}
