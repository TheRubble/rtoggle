﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using RToggle.Shared.Features;

namespace RToggle.Shared.UnitTest
{
    [TestFixture]
    public abstract class FeatureRepositoryTestBase
    {
        public abstract IFeatureRepository GetRepository();
        private IFeatureRepository _repository;

        [SetUp]
        public void SetUp()
        {
            _repository = GetRepository();
            Assert.IsNotNull(_repository);
        }


        [Test]
        public void GetFeatureReturnsDisabledFeatureWhenMissing()
        {
            CheckFeaturesIsNull("MissingFeature", false, false);
        }

        [Test]
        public void GetFeatureReturnsActiveFeature()
        {
            CheckFeatures("TestTrue", true, false, typeof(Feature));
        }

        [Test]
        public void GetFeatureReturnsActiveFeatureCheckingCaseInsensitive()
        {

            CheckFeatures("tEsTtRuE", true, false, typeof(Feature));
        }

        [Test]
        public void TimedFeature_Enabled_Established()
        {
            CheckFeatures("TimedFeatureOne", true, true, typeof(TimedFeature));
        }

        [Test]
        public void TimedFeatureTwo_NotEnabled_NotEstablished()
        {
            CheckFeatures("TimedFeatureTwo", false, false, typeof(TimedFeature));
        }

        [Test]
        public void TestTrueWithEstablished_Enabled_Established()
        {
            CheckFeatures("TestTrueWithEstablished", true, true, typeof(Feature));
        }

        [Test]
        public void TestTrue_Enabled_NotEstablished()
        {
            CheckFeatures("TestTrue", true, false, typeof(Feature));
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void FeatureNameIsNull_ExpectArgumentException()
        {
            CheckFeatures(null, true, false, typeof(Feature));
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void FeatureNameIsEmpty_ExpectArgumentException()
        {
            CheckFeatures(string.Empty, true, false, typeof(Feature));
        }

        protected void CheckFeatures(string name, bool enabled, bool established, Type featureType)
        {
            var subject = _repository;
            var feature = subject.GetFeature(name);
            Assert.IsNotNull(feature);
            Assert.AreEqual(feature.Enabled, enabled);
            Assert.AreEqual(feature.Established, established);
            Assert.That(feature.Name, Is.EqualTo(name).IgnoreCase);
            Assert.IsInstanceOf(featureType, feature);
        }

        protected void CheckFeaturesIsNull(string name, bool enabled, bool established)
        {
            var subject = _repository;
            var feature = subject.GetFeature(name);
            Assert.IsNull(feature);
        }
    }
}
