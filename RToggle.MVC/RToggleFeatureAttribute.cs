﻿using System;
using System.Web.Mvc;
using RToggle.Core;

namespace RToggle.MVC
{
    [AttributeUsage(AttributeTargets.All)]
    public class RToggleFeatureAttribute : FilterAttribute
    {
        public string Feature { get; private set; }

        public RToggleFeatureAttribute(string feature)
        {
			if (string.IsNullOrEmpty (feature)) 
			{
				throw new ArgumentException ("feature");
			}
			this.Feature = feature;
        }
    }

}
