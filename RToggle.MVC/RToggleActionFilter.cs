﻿namespace RToggle.MVC
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Core;
    using Shared;

    /// <summary>
    /// The r toggle action filter.
    /// </summary>
    public class RToggleActionFilter : ActionFilterAttribute
    {

        private readonly IFeatureEngine _featureEngine;
        private readonly Func<IFeature, ActionResult> _onFeatureDisabled;

        public RToggleActionFilter(IFeatureEngine featureEngine, Func<IFeature,ActionResult> onFeatureDisabled)
        {
            if(featureEngine == null)
                throw new ArgumentNullException("featureEngine");

            if (onFeatureDisabled == null)
                throw new ArgumentNullException("onFeatureDisabled");

            this._featureEngine = featureEngine;
            this._onFeatureDisabled = onFeatureDisabled;
        }
        /// <summary>
        /// The on action executing.
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var attributes = from attr in filterContext.ActionDescriptor.GetCustomAttributes(true)
                             where attr.GetType() == typeof(RToggleFeatureAttribute)
                             select (RToggleFeatureAttribute)attr;

            foreach (var attr in attributes)
            {
                var feature = _featureEngine.GetFeature(attr.Feature);
                if (feature.Enabled) continue;


                filterContext.Result =  _onFeatureDisabled(feature);
                break;
            }

        }

    }
}
