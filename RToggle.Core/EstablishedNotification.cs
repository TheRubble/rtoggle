﻿using System;
using RToggle.Shared;
using System.Collections.Generic;

namespace RToggle.Core
{
    
    public class EstablishedNotification : IEstablishedNotification
    {

		private readonly List<IEstablishedObserver> _observers = new List<IEstablishedObserver>();

		public void Register(IEstablishedObserver observer)
		{
			if (observer == null) {
				throw new ArgumentNullException ("observer");
			}

			_observers.Add (observer);
		}

        public void Deregister(IEstablishedObserver observer)
		{
			if (observer == null) {
				throw new ArgumentNullException ("observer");
			}

			if (_observers.Contains (observer)) {
				_observers.Remove (observer);
			}
		}

		public void Notify(IFeature feature){
			_observers.ForEach (o => o.Notify (feature));
		}

	}
}

