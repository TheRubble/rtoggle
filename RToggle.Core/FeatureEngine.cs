﻿using System;
using System.Collections.Generic;
using System.Linq;
using RToggle.Shared.Features;

namespace RToggle.Core
{
    using Shared;

    public class FeatureEngine : IFeatureEngine
    {

        private readonly IEnumerable<IFeatureRepository> _repositories;
        private readonly IEstablishedNotification _establishedNotification;

        public FeatureEngine(IFeatureRepository repository)
            : this(repository, new EstablishedNotification())
        {
            if (repository == null)
            {
                throw new ArgumentNullException("repository");
            }
        }

        public FeatureEngine(IFeatureRepository repository, IEstablishedNotification establishedNotification)
            : this(new[] { repository }, establishedNotification) {}
        
        public FeatureEngine(IFeatureRepository[] repositories) 
            : this(repositories,new EstablishedNotification()) { }

        public FeatureEngine(IFeatureRepository[] repositories,IEstablishedNotification establishedNotification)
        {
            if (repositories == null || !repositories.Any())
            {
                throw new ArgumentNullException("repositories");    
            }

            if (establishedNotification == null)
            {
                throw new ArgumentNullException("establishedNotification");
            }

            _repositories = new List<IFeatureRepository>(repositories);
            _establishedNotification = establishedNotification;
        }

        public IFeature GetFeature(string featureKey)
        {
			if (string.IsNullOrEmpty (featureKey)) 
			{
				throw new ArgumentException ("feature");
			}

            foreach (var feature in _repositories.Select(repository => repository.GetFeature(featureKey)).Where(feature => feature != null))
            {
                if (feature.Established)
                {
                    _establishedNotification.Notify(feature);
                }
                return feature;
            }

            return new Feature(featureKey,false,false);

        }

        public void RegisterForEstablisedNotification(IEstablishedObserver observer)
        {
            _establishedNotification.Register(observer);
        }

        public void DeregisterForEstablishedNotification(IEstablishedObserver observer)
        {
            _establishedNotification.Deregister(observer);
        }
    }
  
}
